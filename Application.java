public class Application{
	public static void main(String[] args){
		Student Jorel = new Student();
		Jorel.name = "Jorel Castro";
		Jorel.program = "Computer Science Technology";
		Jorel.isFullTime = true;
		Jorel.year = 1;
		Jorel.Rscore = 33;
		Jorel.introduceYourself();
		Jorel.talkAboutMe();
		
		Student Andrew = new Student();
		Andrew.name = "Andrew";
		Andrew.program = "Business Management";
		Andrew.isFullTime = false;
		Andrew.year = 2;
		Andrew.Rscore = 26;
		Andrew.introduceYourself();
		Andrew.talkAboutMe();
		
		Student[] section3 = new Student[3];
		section3[0] = Jorel;
		section3[1] = Andrew;
		
		section3[2] = new Student();
		section3[2].name = "Lewis";
		section3[2].program = "Nursing";
		section3[2].isFullTime = true;
		section3[2].year = 3;
		section3[2].Rscore = 35.8;
		
		section3[2].introduceYourself();
		section3[2].talkAboutMe();
	}
}