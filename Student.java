public class Student{
	public String name;
	public String program;
	public boolean isFullTime;
	public int year;
	public double Rscore;
	
	public void introduceYourself(){
		System.out.println("Hello, my name is " + name + " and I'm currently studying in the " + program + " program.");
	}
	public void talkAboutMe(){
		System.out.print("I am currently a " + year);
		if(year == 1){
			System.out.print("st ");
		}else if(year == 2){
			System.out.print("nd ");
		}else if(year == 3){
			System.out.print("rd ");
		}
		System.out.print("year ");
		if(isFullTime){
			System.out.print("full-time ");
		}else{
			System.out.print("part-time ");
		}
		System.out.println("student at Dawson College with an R-score of " + Rscore + ".");
	}
}